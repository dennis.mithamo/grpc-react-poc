package gamestats

import (
	"context"
	"fmt"
	"time"
)

var gameMetadata = GameMetadata{
	TeamAName:  "Manchester United",
	TeamBName:  "Arsenal FC",
	TimePlayed: 1 * 60,
	ExtraTime:  5 * 60,
}

var teamAStats = TeamStats{
	Goals:         1,
	ShotsOnTarget: 4,
	Corners:       3,
	YellowCards:   0,
	RedCards:      0,
	TeamName:      "Manchester United",
}

var teamBStats = TeamStats{
	Goals:         0,
	ShotsOnTarget: 1,
	Corners:       1,
	YellowCards:   2,
	RedCards:      0,
	TeamName:      "Arsenal FC",
}

type Server struct {
	UnimplementedGameStatsServer
}

func (s *Server) MonitorGameStats(_ *EmptyRequest, stream GameStats_MonitorGameStatsServer) error {
	err := getAndSendStats(stream, s)

	timer := time.NewTicker(5 * time.Second)
	for {
		select {
		case <-stream.Context().Done():
			return nil
		case <-timer.C:
			gameMetadata.TimePlayed += 5
			err = getAndSendStats(stream, s)
			if err != nil {
				return fmt.Errorf("something went wrong: %s", err)
			}
		}
	}
}

func (s *Server) GetStats() (*CombinedGameStats, error) {
	return &CombinedGameStats{
		TeamAStats:   &teamAStats,
		TeamBStats:   &teamBStats,
		GameMetadata: &gameMetadata,
	}, nil
}

func (s *Server) GetGameStats(ctx context.Context, _ *EmptyRequest) (*CombinedGameStats, error) {
	return &CombinedGameStats{
		TeamAStats:   &teamAStats,
		TeamBStats:   &teamBStats,
		GameMetadata: &gameMetadata,
	}, nil
}

func getAndSendStats(stream GameStats_MonitorGameStatsServer, server *Server) error {
	stats, err := server.GetStats()
	if err != nil {
		return fmt.Errorf("something went wrong: %s", err)
	}

	err = stream.Send(stats)
	if err != nil {
		return fmt.Errorf("something went wrong: %s", err)
	}

	return err
}

func (s *Server) UpdateTeamStats(ctx context.Context, stats *TeamStats) (*EmptyRequest, error) {
	if stats.TeamName == teamAStats.TeamName {
		teamAStats = *stats
	} else if stats.TeamName == teamBStats.TeamName {
		teamBStats = *stats
	}

	return &EmptyRequest{}, nil
}
