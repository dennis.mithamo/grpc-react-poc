import { useMemo, useState } from 'react'
import { GameStatsClient } from './protos/game_stats.client'
import { GrpcWebFetchTransport } from '@protobuf-ts/grpcweb-transport'
import { EmptyRequest } from './protos/game_stats.ts'

const BASE_API_URL = import.meta.env.VITE_BASE_API_URL

const transport = new GrpcWebFetchTransport({
    baseUrl: BASE_API_URL,
})

const client = new GameStatsClient(transport)

export const App: React.FC = () => {
    const [gameStats, setGameStats] = useState<any>(null)

    const timePlayed = useMemo<string>(() => {
        const timeInSecs = gameStats?.gameMetadata.timePlayed
        return (
            String(Math.floor(timeInSecs / 60)).padStart(2, '0') +
            ' : ' +
            String(timeInSecs % 60).padStart(2, '0')
        )
    }, [gameStats])

    const streamChanges = async () => {
        const stream = client.monitorGameStats(EmptyRequest, {})
        for await (let message of stream.responses) {
            setGameStats(
                (stats: any) => ({ ...stats, ...(message as any) }) as any
            )
        }
    }
    streamChanges()

    const updateStatsManually = async () => {
        setGameStats(null)

        const stats = await client.getGameStats(EmptyRequest)
        setGameStats(stats.response)
    }

    return (
        <div className={'flex flex-col w-full h-full'}>
            <header
                className={
                    'p-3 shadow flex w-full justify-between items-center'
                }
            >
                <h2 className={'font-bold'}>DSB | Gamestats</h2>

                <button
                    onClick={updateStatsManually}
                    className={'bg-green-500 py-2 px-3 text-black'}
                >
                    Refresh manually
                </button>
            </header>

            <main
                className={
                    'h-screen w-full flex flex-col items-center justify-center p-8'
                }
            >
                {gameStats ? (
                    <div className={'w-[450px]'}>
                        <p
                            className={
                                'bg-black text-white p-2 w-full uppercase flex justify-between items-center'
                            }
                        >
                            <span> Time played</span>
                            <span className={'font-bold'}>{timePlayed}</span>
                        </p>
                        <pre
                            className={
                                'text-sm bg-gray-100 text-gray-800 p-8 w-full rounded-lg shadow-sm'
                            }
                        >
                            {JSON.stringify(gameStats, null, 2)}
                        </pre>
                    </div>
                ) : (
                    <>Loading game stats...</>
                )}
            </main>
        </div>
    )
}
