#!/bin/zsh
 protoc -I ./protos ./protos/game_stats.proto \
 --go_out=./go-server/gamestats \
 --go-grpc_out=./go-server/gamestats \
 --ts_out=./react-client/src/protos
