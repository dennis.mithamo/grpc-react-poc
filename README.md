# gRPC + React PoC

This is an experiment to test whether it is possible to use gRPC to query a backend service in a React client application.


## Running it locally

Clone the repo, and checkout into the `main` branch.
The directory structure should match the following:

```bash
$ tree . --filelimit=20
.
├── README.md
├── envoy.yaml
├── go-server
│   ├── gamestats
│   │   ├── game_stats.pb.go
│   │   ├── game_stats_grpc.pb.go
│   │   └── gamestats.go
│   ├── go.mod
│   ├── go.sum
│   └── main.go
├── protos
│   ├── game_stats.proto
│   └── generate_interfaces.sh
└── react-client
    ├── README.md
    ├── index.html
    ├── node_modules  [229 entries exceeds filelimit, not opening dir]
    ├── package-lock.json
    ├── package.json
    ├── postcss.config.js
    ├── public
    │   └── vite.svg
    ├── src
    │   ├── App.tsx
    │   ├── assets
    │   │   └── react.svg
    │   ├── index.css
    │   ├── main.tsx
    │   ├── protos
    │   │   ├── game_stats.client.ts
    │   │   └── game_stats.ts
    │   └── vite-env.d.ts
    ├── tailwind.config.js
    ├── tsconfig.json
    ├── tsconfig.node.json
    └── vite.config.ts

10 directories, 27 files


```

The repo contains:

- `go-server/`: a simple server built in Go to act as the source of data
- `react-client/`: a React + TypeScript client application

To run the demo, it is required that both the server and the client app be run.

### Running the server

```bash
$ cd go-server
$ go mod tidy # ensure dependencies are fetched
$ go run main.go
```

This should output the following on the console:
```bash
[timestamp here] server listening at :9000
```
The server will then be available to listen for requests on `http://localhost:9000`, if all went well


### Proxy the server

The server started above will be served over the `HTTP/2` protocol. `gRPC-web` client apps need to connect to such gRPC servers
via a proxy. The docs recommend using `Envoy` proxy. (See #References below).
The proxy service will listen for connections to `http://localhost:8080`, which the client app will be sending requests to over `HTTP/1.1`, and 
redirect these to the address where the server is actually running, `http://localhost:9000` over `HTTP/2`.

To run Envoy proxy, install it by referencing the docs (see #References below). Then do the following, from the root dir of the repo
```bash
$ envoy -c envoy.yaml
```
The `envoy.yaml` file contains the configuration necessary to redirect requests as desired.


### Running the client
```bash
$ cd react-client
$ echo "VITE_BASE_API_URL=http://localhost:8080" > .env
$ npm install
$ npm run dev
```
The client application will be served on `http://localhost:[PORT]`. Open the application on your favorite browser. 

If all went well, after a couple of seconds, you should see a screen displaying faux game stats on your browser.

PS: The `PORT` is determined by Vite, the bundler being used here, and might differ from system to system, or time to time.

PPS: This has only been tested on Chrome.


## Making changes to the protocol buffer

The `~/protos/game_stats.proto` file contains the high level API that defines the methods the server will implement, 
and therefore the methods the client can invoke on the server. 

A command is run to generate the interfaces for the specific language in which the client and server are implemented.
The shell script `~/protos/generate_interfaces.sh` contains this command, and running it will generate the interfaces
that the server and client should be implemented against.

This command requires that the protocol buffer compiler be installed. See the #References section for instructions.

PS: In this PoC, the compiler used to generate the React client interface is `protobuf-ts`, preferred for its support for TypeScript.


```bash
# in the root of the repository!
$ sh ./protos/generate_interfaces.sh 
```
This should output both the server and client interfaces, and place them in suitable directories for ease of access by the 
respective apps.

It is necessary to restart the server and client after regenerating the interfaces.

### Generate client interfaces only

A copy of the `~/protos/game_stats.proto` file can be placed in the client app directory, in this case as `~/react-client/protos/game_stats.proto`.

An NPM script has been defined in `~/react-client/package.json`, which can be used to generate the client-only interface.

```bash
# in ~/react-client!
npm run proto-gen
```
This should output the client interface and place it inside the same directory as `~/react-client/protos/game_stats.proto`.

## References

- https://grpc.io/
- https://github.com/grpc/grpc-web/tree/master
- https://github.com/grpc/grpc-web/tree/master?tab=readme-ov-file#code-generator-plugins
- https://protobuf.dev/
- https://vitejs.dev
- https://github.com/timostamm/protobuf-ts/tree/main
