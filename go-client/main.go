package main

import (
	"context"
	"gamestatsclient/gamestatsclient"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"time"
)

const (
	PORT = ":9000"
)

func main() {
	conn, err := grpc.NewClient(PORT, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("something went wrong: %v", err)
	}

	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("something went wrong: %v", err)
		}
	}(conn)

	client := gamestatsclient.NewGameStatsClient(conn)

	sampleTeamAStats := &gamestatsclient.TeamStats{
		Goals:         2,
		ShotsOnTarget: 5,
		Corners:       1,
		YellowCards:   3,
		RedCards:      0,
		TeamName:      "Manchester United",
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	_, err = client.UpdateTeamStats(ctx, sampleTeamAStats)
	if err != nil {
		log.Fatalf("something went wrong: %v", err)
	}
}
