package main

import (
	"gamestats/gamestats"
	"google.golang.org/grpc"
	"log"
	"net"
)

const (
	PORT = ":9000"
)

func main() {
	listener, err := net.Listen("tcp", PORT)
	if err != nil {
		log.Fatalf("%v", err)
	}

	server := grpc.NewServer()
	gamestats.RegisterGameStatsServer(server, &gamestats.Server{})

	log.Printf("server listening at %v", PORT)
	if err := server.Serve(listener); err != nil {
		log.Fatalf("%v", err)
	}
}
